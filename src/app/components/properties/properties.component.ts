import {Component, EventEmitter, OnInit} from '@angular/core';
import {LabelType, Options} from 'ng5-slider';
import {HttpServiceService} from '../../service/http-service.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';

declare var $: any;
// const PRODUCT_API = 'https://5eeebeed99b2440016bc08b1.mockapi.io/product';
// const PRODUCT_API = 'https://fxteam-back-end.herokuapp.com/api/product';
const PRODUCT_API = 'https://fxteam-back-end.herokuapp.com/api/partner';
const PARTNER_API = 'https://fxteam-back-end.herokuapp.com/api/partner';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
})
export class PropertiesComponent implements OnInit {

  public properties: Array<any>;
  public term: string;
  typeOptions = ['Khu nghỉ dưỡng', 'Đất nền', 'Nhà phố', 'Căn hộ'];
  public partners: Array<any>;

  constructor(private userService: HttpServiceService) {
  }

  valuePrice: number = 100;
  higValuePrice: number = 300;
  optionsPrice: Options = {
    floor: 100,
    ceil: 10000,
    hideLimitLabels: true,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + 'đ';
        case LabelType.High:
          return value + 'đ';
        default:
          return value + 'đ';
      }
    }
  };

  valueArea: number = 100;
  higValueArea: number = 300;
  optionsArea: Options = {
    floor: 50,
    ceil: 1000,
    hideLimitLabels: true,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + 'm2';
        case LabelType.High:
          return value + 'm2';
        default:
          return value + 'm2';
      }
    }
  };

  products: Array<any>;
  page = 1;

  ngOnInit(): void {
    $(document).ready(function() {
      console.log('testing');
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-yellow',
        radioClass: 'iradio_square-yellow',
        increaseArea: '20%' // optional
      });
      $('.layout-grid').on('click', function() {
        $('.layout-grid').addClass('active');
        $('.layout-list').removeClass('active');
        $('#list-type').removeClass('proerty-th-list');
        $('#list-type').addClass('proerty-th');
      });
      $('.layout-list').on('click', function() {
        $('.layout-grid').removeClass('active');
        $('.layout-list').addClass('active');
        $('#list-type').addClass('proerty-th-list');
        $('#list-type').removeClass('proerty-th');
      });
    });

    this.userService.getAll(PRODUCT_API).subscribe(data => {
      this.products = data;
      this.userService.getAll(PARTNER_API).subscribe(data => {
        this.partners = data;
        console.log(this.partners);
      });
    });
  }

  valuechange(newValue) {
    this.term = newValue;
    if (newValue !== '' || !newValue) {
      this.userService.searchAllColumn(PRODUCT_API, newValue).subscribe(data => {
        this.products = data;
      });
    }
  }
}
